package br.com.g3solutions.eproc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class EventProcessor {

	private static final int PROCESS_ID_INDEX = 0;
	private static final int EVENT_DATE_INDEX = 1;
	private static final int EVENT_TYPE_INDEX = 2;
	private static final int EVENT_DETAIL_INDEX = 3;

	private static final DateTimeFormatter eventDateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	static void process(List<String> rawEvents) {

		List<Process> processes = parseRawEvents(rawEvents);

		processes = sortProcessesById(processes);

		processes.forEach(p -> p.removeUnimportantEvents());

		printOutput(processes);
	}

	private static List<Process> parseRawEvents(List<String> rawEvents) {

		List<Process> proc = new ArrayList<>();
		Process temp = null;
		Long id;
		Event ev;
		boolean idExistente = false;
		for (String rawEvent : rawEvents) {
			String[] eventData = rawEvent.split(";");
			id = Long.parseLong(eventData[PROCESS_ID_INDEX]);
			for (Process p : proc) {
				if (p.getId().equals(id)) {
					idExistente = true;
					temp = p;
					break;
				}
			}
			if (!idExistente) {
				temp = new Process(id);
				ev = new Event();
				ev.setDate(LocalDate.parse(eventData[EVENT_DATE_INDEX], eventDateFormatter));
				ev.setType(EventTypes.byId(eventData[EVENT_TYPE_INDEX]));
				ev.setDetail(eventData[EVENT_DETAIL_INDEX]);
				temp.addEvent(ev);
				proc.add(temp);
			} else {
				ev = new Event();
				ev.setDate(LocalDate.parse(eventData[EVENT_DATE_INDEX], eventDateFormatter));
				ev.setType(EventTypes.byId(eventData[EVENT_TYPE_INDEX]));
				ev.setDetail(eventData[EVENT_DETAIL_INDEX]);
				temp.addEvent(ev);
			}
		}

		return proc;
	}

	private static List<Process> sortProcessesById(List<Process> processes) {
		int n = processes.size();
		for (int i = 0; i < n-1; i++)
			for (int j = 0; j < n-i-1; j++)
				if (processes.get(j+1).getId() < processes.get(j).getId()) {
					Process temp = processes.get(j);
					processes.set(j, processes.get(j+1));
					processes.set(j+1, temp);
				}
		return processes;
	}


	private static void printOutput(List<Process> processes) {
		StringBuilder output = new StringBuilder();

		for (Process process : processes) {

			output.append("Processo: " + process.getId());
			output.append("\n\n");

			for (Event event : process.getEvents()) {
				output.append("Data: " + eventDateFormatter.format(event.getDate()) + "\n");
				output.append("Tipo: " + event.getType().getDescription() + "\n");
				output.append("Detalhamento: " + event.getDetail());
				output.append("\n\n");
			}

			output.append("Qtd. eventos ignorados: " + process.getUnimportantEventsQuantity());
			output.append("\n\n");
		}

		System.out.println(output);
	}
}