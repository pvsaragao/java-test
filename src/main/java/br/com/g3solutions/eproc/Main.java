package br.com.g3solutions.eproc;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List<String> rawEventsSample = new ArrayList();
		rawEventsSample.add("789;07/02/2020;S;Juiz decidiu que o réu é culpado diante das provas apresentadas");
		rawEventsSample.add("123;08/02/2020;JZ;O juiz determinou prazo de 10 dias úteis para proferir a decisão final");
		rawEventsSample.add("654;10/02/2020;AC;Juiz convoca a defesa e a acusação para uma audiência de conciliação");
		rawEventsSample.add("999;10/02/2020;JZ;O juiz irá analisar o processo para verificar se vai aceitar a denúncia");
		rawEventsSample.add("999;11/02/2020;JZ;O juiz irá analisar o processo para verificar se vai aceitar a denúncia");
		rawEventsSample.add("654;22/02/2020;AC;A pedido da acusação, Juiz irá definir nova data para audiência de conciliação");
		rawEventsSample.add("123;27/02/2020;S;Diante da falta de provas o réu foi absolvido");
		
		EventProcessor.process(rawEventsSample);
	}
}
