package br.com.g3solutions.eproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class Process {
	
	private Long id;
	
	private List<Event> events;
	
	private Integer unimportantEventsQuantity;
	
	Process(Long id){
		this.id = id;
		this.events = new ArrayList();
		this.unimportantEventsQuantity = 0;
	}
	
	Long getId() {
		return id;
	}
	
	public Integer getUnimportantEventsQuantity() {
		return unimportantEventsQuantity;
	}
	
	public List<Event> getEvents() {
		return Collections.unmodifiableList(events);
	}
	
	boolean addEvent(Event event) {
		return this.events.add(event);
	}
	
	void removeUnimportantEvents() {
		List<Event> eventosIrrelevantes = new ArrayList();
		for (Event ev : events) {
			if (!ev.getType().isImportant()) {
				eventosIrrelevantes.add(ev);
				this.unimportantEventsQuantity++;
			}
		}
		events.removeAll(eventosIrrelevantes);
	}
}
